<?php

/* 
 * 1.  write a function to find out if the year is a 
 * leap year or not? A number (year) will be passed 
and boolean will be returned.  
 */

$numOfDays = 365;

if(checkLeapYear($numOfDays) !== False)
    echo 'The year is a leap year';
else
    echo 'This is a regular year';

function checkLeapYear($days){    
    if ($days % 2 == 0)
        return TRUE;
        else 
            return FALSE;
}
